import json
from django.http import JsonResponse  # type: ignore
from django.views.decorators.http import require_http_methods  # type: ignore
from .models import AutomobileVO, Salesperson, Customer, Sale
from common.json import ModelEncoder


# Encoders/Decoders here
# VO detail Encoder
class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
        "id"
    ]


# Salesperson Encoder
class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id"
    ]


# Customer Encoder
class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id"
    ]


# Sales List Encoder
class SalesListEncoder(ModelEncoder):
    model = Sale
    properties = [
        "salesperson",
        "customer",
        "automobile",
        "price",
        "id"
    ]
    encoders = {
        "automobile": AutomobileVODetailEncoder(),
        "salesperson": SalespersonEncoder(),
    }

    def get_extra_data(self, o):
        return {
            "customer": o.customer.first_name + " " + o.customer.last_name,
        }


# Sales Detail Encoder
class SalesDetailEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "automobile",
        "customer",
        "salesperson",
    ]
    encoders = {
        "automobile": AutomobileVODetailEncoder(),
    }

    def get_extra_data(self, o):
        return {
            "customer": o.customer.id,
            "salesperson": o.salesperson.id
        }


# list_salespeople for get and post
@require_http_methods(["GET", "POST"])
def list_salespeople(request):
    # list salespeople
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder,
        )

    # create salespeople (salesperson)
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )


# salespeople_detail for get, post, delete
@require_http_methods(["DELETE", "GET", "PUT"])
def salespeople_detail(request, pk):
    # detail salespeople by pk=id
    if request.method == "GET":
        salesperson = Salesperson.objects.get(employee_id=pk)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )

    # delete salespeople by pk=id
    elif request.method == "DELETE":
        count, _ = Salesperson.objects.filter(employee_id=pk).delete()
        return JsonResponse({"deleted": count > 0})

    # update/put salespeople by pk=id
    else:
        content = json.loads(request.body)
        Salesperson.objects.filter(employee_id=pk).update(**content)
        salesperson = Salesperson.objects.get(employee_id=pk)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )


# list_customers for get and post
@require_http_methods(["GET", "POST"])
def list_customers(request):
    # list customers
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )

    # create customers
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


# customer_detail for get, put, delete
@require_http_methods(["DELETE", "GET", "PUT"])
def customers_detail(request, pk):
    # detail customer by pk=id
    if request.method == "GET":
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )

    # delete customer by pk=id
    elif request.method == "DELETE":
        count, _ = Customer.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

    # update/put customer by pk=id
    else:
        content = json.loads(request.body)
        Customer.objects.filter(id=pk).update(**content)
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=SalespersonEncoder,
            safe=False,
        )


# list_sales for get and post
@require_http_methods(["GET", "POST"])
def list_sales(request):
    # list sales
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesListEncoder,
        )

    # create sales
    else:
        content = json.loads(request.body)

        # try and create vin
        try:
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile vin"},
                status=400,
            )

        # try and create salesperson_id
        try:
            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(id=salesperson_id)
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson id"},
                status=400,
            )

        # try and create a customer id
        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(pk=customer_id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer id"},
                status=400,
            )

        # create the actual sale
        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SalesDetailEncoder,
            safe=False,
        )


# sales_detail for  get, put, delete
@require_http_methods(["DELETE", "GET", "PUT"])
def sales_detail(request, pk):
    # list sale by pk
    if request.method == "GET":
        automobile = Sale.objects.get(id=pk)
        customer = Sale.objects.get(id=pk)
        salesperson = Sale.objects.get(id=pk)
        return JsonResponse(
            automobile,
            customer,
            salesperson,
            encoder=SalesDetailEncoder,  # type: ignore
            safe=False,  # type: ignore
        )

    # delete sale by pk
    elif request.method == "DELETE":
        count, _ = Sale.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

    # update/put sale by pk
    else:
        content = json.loads(request.body)

        # update vin
        try:
            if "automobile" in content:
                automobile = AutomobileVO.objects.get(content["automobile"])
                content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile vin"},
                status=400,
            )

        # update customer
        try:
            if "customer" in content:
                customer = Customer.objects.get(content["customer"])
                content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer id"},
                status=400,
            )

        # update salesperson
        try:
            if "salesperson" in content:
                salesperson = Salesperson.objects.get(content["salesperson"])
                content["salesperson"] = salesperson
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson id"},
                status=400,
            )

        # apply changes and return detail view
        Sale.objects.filter(id=pk).update(**content)
        sale = Sale.objects.get(id=pk)
        return JsonResponse(
            sale,
            encoder=SalesDetailEncoder,
            safe=False,
        )
