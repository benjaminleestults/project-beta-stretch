from django.db import models
from django.urls import reverse


# Create your models here.
class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.PositiveSmallIntegerField()

    def get_api_url(self):
        return reverse("api_show_technicians", kwargs={"pk": self.pk})


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17)
    sold = models.CharField(max_length=100)


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=100)
    status = models.CharField(max_length=100, default="current")
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(
        Technician, related_name="appointments", on_delete=models.CASCADE, null=True
    )
    vip = models.CharField(max_length=3, default="No")

    def get_api_url(self):
        return reverse("api_show_appointments", kwargs={"pk": self.pk})

    def save(self, *args, **kwargs):
        if AutomobileVO.objects.filter(vin=self.vin).exists():
            self.vip = "Yes"
        else:
            self.vip = "No"

        super().save(*args, **kwargs)
