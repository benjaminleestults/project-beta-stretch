import { useEffect, useState } from 'react';

function ListManufactures() {
    const [manufList, setManuf] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/');

        if (response.ok) {
            const data = await response.json();
            setManuf(data.manufacturers);
        }
    }

    useEffect(() => {
        getData()
    }, []);

    return (
        <div>
            <h1>Manufacturers</h1>
            <table className="table table-striped table align-middle" style={{ width: '100%', tableLayout: 'fixed' }}>
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody >
                    {manufList.map(manuf => {
                        return (
                            <tr style={{ height: '60px' }} key={manuf.href}>
                                <td>{manuf.name}</td>
                            </tr>
                        );
                    })}

                </tbody>
            </table>
        </div>
    )
}
export default ListManufactures;
