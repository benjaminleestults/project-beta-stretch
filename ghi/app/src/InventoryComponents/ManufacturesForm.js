import React, { useState, useEffect } from 'react';

function ManufacturerForm() {
    // showSuccess is a state used to show if form is submitted succesfully
    const [showSuccess, setShowSuccess] = useState(false);
    const [manuf, setManuf] = useState({ name: '' });

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = "http://localhost:8100/api/manufacturers/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(manuf),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        try {
            const response = await fetch(url, fetchConfig);

            if (response.ok) {
                setManuf({ name: '' });
                setShowSuccess(true);
            }
        } catch (error) {
            console.error('Failed to submit:', error);
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setManuf({
            ...manuf,
            [inputName]: value
        });
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Manufacturer</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={manuf.name} placeholder="name" required type="text" name="name" id="name" className="form-control" autoComplete="off" />
                            <label htmlFor="name">Manufacturer name</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                    {showSuccess && (
                        <div className="alert alert-success" role="alert">
                            Form submitted successfully!
                        </div>
                    )}
                </div>
            </div>
        </div>
    )
}
export default ManufacturerForm;
