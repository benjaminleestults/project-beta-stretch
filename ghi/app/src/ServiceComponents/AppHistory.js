import { useEffect, useState } from 'react';

function AppHistory() {
    const [vin, setVin] = useState('');
    const [appsList, setApps] = useState([])
    const [selectedAppsList, setSelectedApps] = useState([])

    const getData = async () => {
        const url = "http://localhost:8080/api/appointments";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setApps(data.appointments);
        }
    }

    useEffect(() => {
        getData();
    }, []
    );

    const handleSubmit = async (event) => {
        event.preventDefault();
        let tempList = [];
        for (let app of appsList) {
            if (app.vin === vin) {
                tempList.push(app);
            }
        }
        setSelectedApps(tempList);
    }

    const handleValChange = (e) => {
        setVin(e.target.value);
    }

    return (
        <div>
            <h1>Service History</h1>
            <form onSubmit={handleSubmit}>
                <label htmlFor="search-input"></label>
                <input onChange={handleValChange} value={vin} type="search" id="search-input" name="query" placeholder="Search by VIN.." />
                <button type="submit">Search</button>
            </form>
            {selectedAppsList.length > 0 ? (
                <table className="table table-striped table align-middle" style={{ width: '100%', tableLayout: 'fixed' }}>
                    <thead>
                        <tr style={{ height: '60px' }}>
                            <th>VIN</th>
                            <th>Is VIP?</th>
                            <th>Customer</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {selectedAppsList.map((app) => (
                            <tr key={app.href}>
                                <td>{app.vin}</td>
                                <td>{app.vip}</td>
                                <td>{app.customer}</td>
                                <td>{app.date_time.split('T')[0]}</td>
                                <td>{app.date_time.split('T')[1]}</td>
                                <td>{app.technician.first_name} {app.technician.last_name}</td>
                                <td>{app.reason}</td>
                                <td>{app.status}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            ) : (
                <p>No services found.</p>
            )}
        </div>
    )
}
export default AppHistory;
