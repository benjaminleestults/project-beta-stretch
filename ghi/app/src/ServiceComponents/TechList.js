import { useEffect, useState } from 'react';

function TechList() {
    const [techslists, setTechs] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/technicians/');

        if (response.ok) {
            const data = await response.json();
            setTechs(data.technicians);
        }
    }

    useEffect(() => {
        getData()
    }, []);



    return (
        <div>
            <h1>Technicians</h1>
            <table className="table table-striped table align-middle" style={{ width: '100%', tableLayout: 'fixed' }}>
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody >
                    {techslists.map(tech => {
                        return (
                            <tr style={{ height: '60px' }} key={tech.href}>
                                <td>{tech.employee_id}</td>
                                <td>{tech.first_name}</td>
                                <td>{tech.last_name}</td>
                            </tr>

                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default TechList;
