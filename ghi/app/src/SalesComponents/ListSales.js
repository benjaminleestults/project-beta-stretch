import React, { useEffect, useState } from "react";


const ListSales = () => {

    const [sales, setSales] = useState([]);

    const fetchSales = async () => {
        try {
            const response = await fetch("http://localhost:8090/api/sales/");
            if (!response.ok) throw new Error("Failed to fetch sales");
            const data = await response.json();
            setSales(data.sales);
            } catch (error) {
            console.error(error.message);
        }
    };

    useEffect(() => {
      fetchSales();
    }, []);

    return (
        <div className="container mt-4">
            <div className="text-center">
                <h2>Available Sales</h2>
                <table className="table table-striped table-dark">
                    <thead>
                        <tr>
                            <th scope="col">Salesperson Name</th>
                            <th scope="col">Salesperson Employee ID</th>
                            <th scope="col">Customer</th>
                            <th scope="col">VIN</th>
                            <th scope="col">Price</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {sales.map(sale => {
                            return (
                                <tr key={sale.id}>
                                    <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                    <td>{sale.salesperson.employee_id}</td>
                                    <td>{sale.customer}</td>
                                    <td>{sale.automobile.vin}</td>
                                    <td>${sale.price}</td>
                                    <td>{sale.automobile.sold ? "SOLD" : "AVAILABLE"}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    );
  };


export default ListSales;
