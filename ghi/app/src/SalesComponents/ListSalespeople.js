import React, { useEffect, useState } from "react";
import SalespersonForm from "./../SalesComponents/SalespersonForm";


const ListSalespeople = () => {

  // salespeople data
  const [salespeople, setSalespeople] = useState([]);

  async function fetchSalespeople() {

    try {
      const response = await fetch("http://localhost:8090/api/salespeople/");

      if (response.ok) {
        const {salespeople} = await response.json();
        setSalespeople(salespeople);
      } else {
        console.error('Failed to fetch Salespeople')
      }

    } catch (error) {
      console.error(error.message);
    };
  };

  useEffect(() => {
    fetchSalespeople();
  }, []);

  if (salespeople === undefined) {
    return null;
  }

  return (
    <div className="container mt-4">
      <div className="text-center">
        <h2>Sales Representatives</h2>
          <table className="table table-striped table-dark">
            <thead>
              <tr>
                <th scope="col">First Name</th>
                <th scope="col">Last Name</th>
                <th scope="col">Employee ID</th>
              </tr>
            </thead>
            <tbody>
              {salespeople.map(salesperson => {
                return (
                  <tr key={salesperson.id}>
                    <td>{salesperson.first_name}</td>
                    <td>{salesperson.last_name}</td>
                    <td>{salesperson.employee_id}</td>
                  </tr>
                );}
              )}
            </tbody>
          </table>
      </div>
    </div>
  );
};


export default ListSalespeople;
