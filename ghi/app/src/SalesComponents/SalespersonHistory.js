import React, { useEffect, useState } from "react";


const SalespersonHistory = () => {

    const [salespeople, setSalespeople] = useState([]);
    const [sales, setSales] = useState([]);
    const [salesperson, setSalesperson] = useState('');

    // salesperson change handler
    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    };

    const fetchSalespeople = async () => {
        try {

            const response = await fetch("http://localhost:8090/api/salespeople/");
            if (response.ok) {
                const data = await response.json();
                setSalespeople(data.salespeople);
            } else {
                console.error('Failed to fetch Salespeople')
            }

        } catch (error) {

          console.error(error.message);

        }

    };

    const fetchSales = async () => {
        try {
            const response = await fetch("http://localhost:8090/api/sales/")
            if (response.ok) {
                const data = await response.json();
                setSales(data.sales);
            } else {
                console.error('Failed to fetch Sales')
            }

        } catch (error) {

          console.error(error.message);

        }
    };

    useEffect(() => {
        fetchSales();
        fetchSalespeople();
    }, []);

    return (
        <div className="container mt-4">
            <h2>Salesperson History</h2>
            <select onChange={handleSalespersonChange} required name="salesperson" id="salesperson" className="form-select" value={salesperson} >
                <option>Choose a Salesperson!</option>
                {salespeople.map(salesperson => {
                    return (
                        <option key={salesperson.employee_id} value={salesperson.employee_id}>{salesperson.first_name} {salesperson.last_name}</option>
                    )
                })}
            </select>
            <table className="table table-striped table-dark text-center">
                <thead>
                    <tr>
                        <th scope="col">Salesperson</th>
                        <th scope="col">Customer</th>
                        <th scope="col">VIN</th>
                        <th scope="col">Price</th>
                        <th scope="col">Purchased?</th>
                    </tr>
                </thead>
                <tbody>
                {sales.filter((sale) => sale.salesperson.employee_id === salesperson)
                .map((sale) => (
                    <tr key={sale.id}>
                        <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                        <td>{sale.customer}</td>
                        <td>{sale.automobile.vin}</td>
                        <td>${sale.price}</td>
                        <td>{sale.automobile.sold ? "Yes" : "No"}</td>
                    </tr>
                ))}
                </tbody>
            </table>
        </div>
      );
    };


export default SalespersonHistory;
