import React, { useEffect, useState } from "react";
import CustomerForm from "./../SalesComponents/CustomerForm";


const ListCustomers = () => {
    const [customers, setCustomer] = useState([]);

    const fetchCustomer = async () => {
        try {
            const response = await fetch("http://localhost:8090/api/customers/");

            if (response.ok) {
                const {customers} = await response.json();
                setCustomer(customers);
            } else {
                console.error('Failed to fetch Customers')
            }
        } catch (error) {
          console.error(error.message);
        }
    };

    useEffect(() => {
        fetchCustomer();
    }, []);

    return (
        <div className="container mt-4">
            <div className="text-center">
                <h2>Customers</h2>
                    <table className="table table-striped table-dark">
                        <thead>
                            <tr>
                                <th scope="col">First Name</th>
                                <th scope="col">Last Name</th>
                                <th scope="col">Phone Number</th>
                                <th scope="col">Address</th>
                            </tr>
                        </thead>
                        <tbody>
                            {customers.map(customer => {
                                return (
                                    <tr key={customer.id}>
                                        <td>{customer.first_name}</td>
                                        <td>{customer.last_name}</td>
                                        <td>{customer.phone_number}</td>
                                        <td>{customer.address}</td>
                                    </tr>
                                );}
                            )}
                        </tbody>
                    </table>
            </div>
        </div>
    );
};


export default ListCustomers;
