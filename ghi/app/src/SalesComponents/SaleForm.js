import React, { useEffect, useState } from 'react';


function SaleForm() {

    const [automobiles, setAutomobiles] = useState([])
    const [salespeople, setSalespeople] = useState([])
    const [customers, setCustomers] = useState([])

    // automobile handler
    const [automobile, setAutomobile] = useState('');
    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    };

    // salesperson handler
    const [salesperson, setSalesperson] = useState('');
    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    };

    // customer handler
    const [customer, setCustomer] = useState('');
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    };

    // price handler
    const [price, setPrice] = useState('');
    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    };

    // submit handler
    const handleSubmit = async (event) => {
        event.preventDefault();

        const status = { sold: true }

        const data = {};

        data.automobile = automobile;
        data.salesperson = salesperson;
        data.customer = customer;
        data.price = price;

        // if post = create
        const salesUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        // if put = update
        const inventoryAutomobileUrl = `http://localhost:8100/api/automobiles/${automobile}/`;
        const putConfig = {
            method: "put",
            body: JSON.stringify(status),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const inventoryResponse = await fetch(inventoryAutomobileUrl, putConfig)
        const salesResponse = await fetch(salesUrl, fetchConfig);
        if (salesResponse.ok && inventoryResponse.ok) {
            setAutomobile('');
            setSalesperson('');
            setCustomer('');
            setPrice('');
        }
    }

    const fetchData = async () => {

        const automobilesUrl = 'http://localhost:8100/api/automobiles/';
        const customersUrl = 'http://localhost:8090/api/customers/';
        const salespeopleUrl = 'http://localhost:8090/api/salespeople/';

        const automobilesResponse = await fetch(automobilesUrl);
        const customersResponse = await fetch(customersUrl)
        const salespeopleResponse = await fetch(salespeopleUrl)

        if (automobilesResponse.ok && customersResponse.ok && salespeopleResponse.ok) {

            const automobilesData = await automobilesResponse.json();
            setAutomobiles(automobilesData.autos)

            const customersData = await customersResponse.json();
            setCustomers(customersData.customers)

            const salespeopleData = await salespeopleResponse.json();
            setSalespeople(salespeopleData.salespeople)

        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-3">
                    <h1>Add a New Sale</h1>
                    <form onSubmit={handleSubmit} id="create-sales-form" autoComplete="off">
                        <div className="mb-3">
                            <select onChange={handleAutomobileChange} required name="automobile" id="automobile" className="form-select" value={automobile}>
                                <option>Choose an Automobile...</option>
                                {automobiles.map((automobile) => {
                                    if (automobile["sold"] === false)
                                        return (
                                            <option key={automobile.vin} value={automobile.vin}>
                                                {automobile.model.manufacturer.name} {automobile.model.name} VIN:{automobile.vin}
                                            </option>
                                        )
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleSalespersonChange} required name="salesperson" id="salesperson" className="form-select" value={salesperson}>
                                <option>Choose a Sales Representative...</option>
                                {salespeople.map((salesperson) => {
                                    if (salespeople !== undefined)
                                        return (
                                            <option key={salesperson.employee_id} value={salesperson.employee_id}>
                                                {salesperson.first_name} {salesperson.last_name}
                                            </option>
                                        )
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleCustomerChange} required name="customer" id="customer" className="form-select" value={customer}>
                                <option>Select an interested Customer...</option>
                                {customers.map((customer) => {
                                    return (
                                        <option key={customer.id} value={customer.id}>
                                            {customer.first_name} {customer.last_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePriceChange} placeholder="price" required type="number" name="price" id="price" className="form-control" value={price} />
                            <label htmlFor="price">Enter the Price of the sale...</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}


export default SaleForm;
