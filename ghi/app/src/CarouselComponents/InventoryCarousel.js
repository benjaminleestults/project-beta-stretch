import React, { useEffect, useState } from 'react';
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';


function InventoryCarousel() {

    const [automobiles, setAutomobiles] = useState([]);

    const fetchAutomobiles = async () => {

        try {

            const response = await fetch("http://localhost:8100/api/automobiles/");

            if (response.ok) {
                const data = await response.json();
                setAutomobiles(data.autos);
            } else {
                console.error('Failed to fetch Automobiles')
            }

        } catch (error) {

          console.error(error.message);

        }
    };

    useEffect(() => {
      fetchAutomobiles();
    }, []);

  return (
    <Carousel>
    {automobiles.map((automobile) =>
      <div>
        <img src={automobile.model.picture_url} alt={automobile.model.name} />
        <p className="legend">{automobile.year} {automobile.model.manufacturer.name} {automobile.model.name}</p>
      </div>
    )}
    </Carousel>
  );
};


export default InventoryCarousel;
